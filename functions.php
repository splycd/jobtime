<?php
function getInclude($file){
    include BASE_PATH.'app/includes/'.$file;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getWallpapers(){
	$wallpapers = array(
	    array('wallpaper-0.jpg','Jason Blackeye','https://unsplash.com/@jeisblack'),
	    array('wallpaper-1.jpg','George Tsapakis','https://unsplash.com/@typhoeus'),
	    array('wallpaper-2.jpg','Clark Van Der Beken','https://unsplash.com/@snaps_by_clark'),
	    array('wallpaper-3.jpg','Fatih','https://unsplash.com/@gozlukluf'),
	    array('wallpaper-4.jpg','Paweł Czerwiński','https://unsplash.com/@pawel_czerwinski'),
	    array('wallpaper-5.jpg','Paweł Czerwiński','https://unsplash.com/@pawel_czerwinski'),
		array('wallpaper-6.jpg','Mitchell Luo','https://unsplash.com/@mitchel3uo'),
		array('wallpaper-7.jpg','Mitchell Luo','https://unsplash.com/@mitchel3uo'),
		array('wallpaper-8.jpg','NASA','https://unsplash.com/@nasa'),
		array('wallpaper-9.jpg','NASA','https://unsplash.com/@nasa'),
		array('wallpaper-10.jpg','NASA','https://unsplash.com/@nasa'),
		array('wallpaper-11.jpg','NASA','https://unsplash.com/@nasa'),
		array('wallpaper-12.jpg','NASA','https://unsplash.com/@nasa'),
	);
	$wallpaperSet = rand(0, 12);

	return array($wallpapers[$wallpaperSet][0],$wallpapers[$wallpaperSet][1],$wallpapers[$wallpaperSet][2]);
}

function timeago($time){
    $time_difference = time() - $time;

    if( $time_difference < 1 ) { return 'less than 1 second ago'; }
    $condition = array( 12 * 30 * 24 * 60 * 60 =>  'year',
                       30 * 24 * 60 * 60       =>  'month',
                       24 * 60 * 60            =>  'day',
                       60 * 60                 =>  'hour',
                       60                      =>  'minute',
                       1                       =>  'second'
                      );

    foreach( $condition as $secs => $str )
    {
        $d = $time_difference / $secs;

        if( $d >= 1 )
        {
            $t = round( $d );
            return $t . ' ' . $str . ( $t > 1 ? 's' : '' ) . ' ago';
        }
    }
}

function checkReadOnly(){
	if($GLOBALS[conf]['general.readOnlyMode'] == true){
		return true;
	}else{
		return false;
	}
}

function checkAdmin(){
    $result = $GLOBALS['database']->get('teamAssoc','authority',[
        'userid'=>$_SESSION['userid'],
        'teamid'=>$_SESSION['teamid']
    ]);
    if($result == 0){
        return true;
    }else{
        return false;
    }
}

function checkAuthority($required){
    $authority = $GLOBALS['database']->get('users','authority',[
        'userid'=>$_SESSION['userid']
    ]);

    if($required < $authority){
        return false;
    }else{
        return true;
    }
}

function checkVerified(){
    $result = $GLOBALS['database']->get('users','verified',[
        'userid'=>$_SESSION['userid']
    ]);
    if($result == 0){
        return false;
    }else{
        return true;
    }
}

function checkLogin(){
    $_SESSION['loginUrl'] = $_SERVER['REQUEST_URI'];
    if(isset($_SESSION['userid'])){
        //Check if userid exists in the database
        $count = $GLOBALS['database']->count('users',[
            'userid'=>$_SESSION['userid']
        ]);
        if($count !== 1){
            //User ID is set but doesn't seem to exist in the Database. Send back to login with an error message.
            $_SESSION['notification'] = array(
                'type' => 'modal',
                'style' => 'error',
                'title' => 'Whoops!',
                'content' => 'An error occured with your User ID.',
            );
            setcookie('userid', NULL, time() - (86400 * 365), "/");
            $_SESSION['userid'] = NULL;
			$_SESSION['teamid'] = NULL;
            header('Location: /login');
            exit;
        }else{
			$teamid = $GLOBALS['database']->get('users','teamid',['userid'=>$_SESSION['userid']]);
			$_SESSION['teamid'] = $teamid;

			$verifyCount = $GLOBALS['database']->count('userVerify',['userid'=>$_SESSION['userid']]);
			if($verifyCount > 0){
				header('Location: /verify');
	            exit;
			}
		}
    }else{
        //User ID Session is not set, direct back to login screen.
        $_SESSION['userid'] = NULL;
        $_SESSION['teamid'] = NULL;
        header('Location: /login');
        exit;
    }
}

function checkTeam(){
	/*
    if(isset($_SESSION['teamid'])){
        $count = $GLOBALS['database']->count('teams',[
            'teamid'=>$_SESSION['teamid']
        ]);
        $assocCount = $GLOBALS['database']->count('teamAssoc',[
            'userid'=>$_SESSION['userid'],
            'teamid'=>$_SESSION['teamid']
        ]);
        if($count == false || $assocCount == false){
            //Team ID is set but doesn't seem to exist in the Database. Send back to team setup with an error message.
            $_SESSION['teamid'] = NULL;
            setcookie('teamid', NULL, time() - (86400 * 365), "/");
            header('Location: /account');
            exit;
        }
    }else{
        //Team ID Session is not set, direct back to team setup screen.
        $_SESSION['teamid'] = NULL;
        setcookie('teamid', '', time() - (86400 * 365), "/");
        header('Location: /account');
        exit;
    }
	*/
}

function checkSubscription(){
	$count = $GLOBALS['database']->count('subscriptions',['teamid'=>$_SESSION['teamid']]);
	if($count < 1){
		return 'free';
	}else{
		return $GLOBALS['database']->get('subscriptions','plan',['teamid'=>$_SESSION['teamid']]);
	}
}

function getGravatar($email){
    $gravatarHash = md5(strtolower(trim($email)));
    return 'https://www.gravatar.com/avatar/'.$gravatarHash.'?s=100&d=retro';
}

function teamSettings(){
    $settingsJson = $GLOBALS['database']->get('teams','settings',['teamid'=>$_SESSION['teamid']]);
    return json_decode($settingsJson, true);
}
?>
