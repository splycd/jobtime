<?php
$CacheVersion = 'dev17';

use Medoo\Medoo;
use Noodlehaus\Config;
use Noodlehaus\Parser\Json;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

define('BASE_PATH',dirname($_SERVER['DOCUMENT_ROOT']).'/');
session_start();

require BASE_PATH.'vendor/autoload.php';

$conf = new Config(BASE_PATH.'config.ini');

// Initialize PHP Mailer with Mailgun SMTP
$mail = new PHPMailer(true);
$mail->SMTPDebug = 0;
$mail->isSMTP();
$mail->Host       = $conf['mail.host'];
$mail->SMTPAuth   = $conf['mail.SMTPAuth'];
$mail->Username   = $conf['mail.username'];
$mail->Password   = $conf['mail.password'];
$mail->SMTPSecure = $conf['mail.SMTPSecure'];
$mail->Port       = $conf['mail.port'];

// Initialize Medoo
$database = new Medoo([
    'database_type' => $conf['database.type'],
    'database_name' => $conf['database.database'],
    'server' => $conf['database.server'],
    'username' => $conf['database.username'],
    'password' => $conf['database.password']
]);

$stripe = new \Stripe\StripeClient($conf['stripe.secretKey']);

date_default_timezone_set('Europe/London');

require_once BASE_PATH.'functions.php';
require_once BASE_PATH.'components.php';

if(isset($_COOKIE['token'])){
	$tokenCount = $database->count('loginToken',['token'=>$_COOKIE['token']]);
	if($tokenCount !== 1){
		$_SESSION['userid'] = NULL;
		$_SESSION['teamid'] = NULL;
		setcookie('token', NULL, time() - (86400 * 365), "/");
	}else{
		$token = $database->get('loginToken',[
			'[>]users'=>'userid'
		],[
			'users.userid',
			'users.teamid'
		],[
			'loginToken.token'=>$_COOKIE['token']
		]);
		$_SESSION['userid'] = $token['userid'];
		$_SESSION['teamid'] = $token['teamid'];
	}
}else{
	$_SESSION['userid'] = null;
	$_SESSION['teamid'] = null;
}
?>
