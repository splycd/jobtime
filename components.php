<?php
function renderClient($client){
    $jobs = $GLOBALS['database']->count('jobs',[
        'clientid'=>$client['clientid']
    ]);
    if($jobs === 1){
        $jobs = '1 Job';
    }else{
        $jobs = $jobs.' Jobs';
    }
    echo'
    <a href="/clients/'.$client['clientid'].'">
        <div class="object-title">'.$client['name'].'</div>
        <div class="object-attr">'.$jobs.'</div>
    </a>
    ';
}
function renderClientTable($client){
    $jobs = $GLOBALS['database']->count('jobs',[
        'clientid'=>$client['clientid']
    ]);
    if($jobs === 1){
        $jobs = '1 Job';
    }else{
        $jobs = $jobs.' Jobs';
    }

	$archivedJobs = $GLOBALS['database']->count('jobArchives',[
        'clientid'=>$client['clientid']
    ]);
    if($archivedJobs === 0){
        $archivedJobs = '';
    }elseif($archivedJobs === 1){
        $archivedJobs = ' (1 Archived)';
    }else{
        $archivedJobs = ' ('.$archivedJobs.' Archived)';
    }

	if(checkAdmin()){
		echo'
	    <div class="object-table__row object-table__row--controls">
			<a href="/clients/'.$client['clientid'].'" class="object-table__row__body object-table__row__body--link object-table__row__body--clients">
				<div class="object-table-clients__title">
					'.$client['name'].'
				</div>
				<div class="object-table-clients__jobs">
					'.$jobs.$archivedJobs.'
				</div>
			</a>
			<button class="object-table__row__controls" type="button" data-toggle="dropdown"><i class="fas fa-ellipsis-v fa-fw"></i></button>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item" href="#"><span class="text--red"><i class="fas fa-trash fa-fw"></i>Delete</span></a>
			</div>
		</div>
	    ';
	}else{
		echo'
	    <div class="object-table__row">
			<a href="/clients/'.$client['clientid'].'" class="object-table__row__body object-table__row__body--link object-table__row__body--clients">
				<div class="object-table-clients__title">
					'.$client['name'].'
				</div>
				<div class="object-table-clients__jobs">
					'.$jobs.'
				</div>
			</a>
		</div>
	    ';
	}
}
function renderJob($job){

    $budget = $job['budget'];
    $budgetSeconds = $job['budget'] * 60 * 60;

    $logs = $GLOBALS['database']->select('logs','seconds [Int]',['jobid'=>$job['jobid']]);

    $seconds = array_sum($logs);
    $hours = floor($seconds / 60 / 60);
    $mins = floor(($seconds % 3600) / 60);

    $time = array();

    if($hours > 0){
        array_push($time, $hours.'h');
    }
    if($mins > 0){
        array_push($time, $mins.'m');
    }

    $time = implode(' ',$time);

    if($seconds < 1){
        $time = '0m';
    }

    $percentage = ($seconds * 100)/$budgetSeconds;

    if($percentage > 100){
        $percentage = 100;
    }

    if($percentage > 90){
        $barColour = 'object__progress-bar--red';
        $textColour = 'text--red';
    }elseif($percentage > 80){
        $barColour = 'object__progress-bar--orange';
        $textColour = 'text--orange';
    }else{
        $barColour = 'object__progress-bar--yellow';
        $textColour = 'text--yellow';
    }

    if($job['budget'] > 0){
        echo'
        <a href="/jobs/'.$job['jobid'].'" class="object--progress-bar">
            <div class="object-title">'.$job['jobname'].'</div>
            <div class="object-attr">'.$job['clientname'].'</div>
            <div class="object-attr"><span class="'.$textColour.'">'.$time.'</span> / '.$budget.'h</div>
            <div class="object__progress-bar '.$barColour.'"><div style="width: '.$percentage.'%"></div></div>
        </a>
        ';
    }else{
        echo'
        <a href="/jobs/'.$job['jobid'].'">
            <div class="object-title">'.$job['jobname'].'</div>
            <div class="object-attr">'.$job['clientname'].'</div>
            <div class="object-attr">'.$time.'</div>
        </a>
        ';
    }
}

function renderJobTable($job){

    $budget = $job['budget'];
    $budgetSeconds = $job['budget'] * 60 * 60;

    $logs = $GLOBALS['database']->select('logs','seconds [Int]',['jobid'=>$job['jobid']]);

    $seconds = array_sum($logs);
    $hours = floor($seconds / 60 / 60);
    $mins = floor(($seconds % 3600) / 60);

    $time = array();

    if($hours > 0){
        array_push($time, $hours.'h');
    }
    if($mins > 0){
        array_push($time, $mins.'m');
    }

    $time = implode(' ',$time);

    if($seconds < 1){
        $time = '0m';
    }

    $percentage = ($seconds * 100)/$budgetSeconds;

    if($percentage > 100){
        $percentage = 100;
    }

    if($percentage > 90){
        $barColour = 'red';
        $textColour = 'text--red';
    }elseif($percentage > 80){
        $barColour = 'orange';
        $textColour = 'text--orange';
    }else{
        $barColour = 'yellow';
        $textColour = 'text--yellow';
    }

	if($job['budget'] > 0){
		$timeModule = '
		<div class="object-table-jobs__time object-table-jobs__time--progress-bar">
			<div class="object-table-jobs__progress-bar object-table-jobs__progress-bar--'.$barColour.'"><div style="width: '.$percentage.'%"></div></div>
			<div class="object-table-jobs__time__label"><span class="'.$textColour.'">'.$time.'</span> / '.$budget.'h</div>
		</div>
		';
	}else{
		$timeModule = '
		<div class="object-table-jobs__time">
			<div class="object-table-jobs__time__label">'.$time.'</div>
		</div>
		';
	}

	if(checkAdmin()){
		echo'
		<div class="object-table__row object-table__row--controls">
			<a href="/jobs/'.$job['jobid'].'" class="object-table__row__body object-table__row__body--link object-table__row__body--jobs">
				<div class="object-table-jobs__title">
					'.$job['jobname'].'
				</div>
				<div class="object-table-jobs__client">
					'.$job['clientname'].'
				</div>
				'.$timeModule.'
			</a>
			<button class="object-table__row__controls" type="button" data-toggle="dropdown"><i class="fas fa-ellipsis-v fa-fw"></i></button>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item" href="#" onclick="archiveJob('."'".$job['jobid']."'".')"><i class="fas fa-archive fa-fw"></i>Archive</a>
				<a class="dropdown-item" href="#"><span class="text--red"><i class="fas fa-trash fa-fw"></i>Delete</span></a>
			</div>
		</div>
		';
	}else{
		echo'
		<div class="object-table__row">
			<a href="/jobs/'.$job['jobid'].'" class="object-table__row__body object-table__row__body--link object-table__row__body--jobs">
				<div class="object-table-jobs__title">
					'.$job['jobname'].'
				</div>
				<div class="object-table-jobs__client">
					'.$job['clientname'].'
				</div>
				'.$timeModule.'
			</a>
		</div>
		';
	}
}

function renderLog($log){

    $seconds = $log['seconds'];
    $hours = floor($seconds / 60 / 60);
    $mins = floor(($seconds % 3600) / 60);

    $time = array();

    if($hours > 0){
        array_push($time, $hours.'h');
    }
    if($mins > 0){
        array_push($time, $mins.'m');
    }

    $time = implode(' ',$time);

	$functionID = "'".$log['id']."'";

    if(empty($log['userid'])){
        echo'
        <a href="#" onclick="openLogItem('.$functionID.')">
            <div class="log-title">'.$log['jobname'].'</div>
            <div class="log-client">'.$log['clientname'].'</div>
            <div class="log-description">'.$log['description'].'</div>
            <div class="log-time">'.$time.'</div>
        </a>
        ';
    }else{
        $user = $GLOBALS['database']->get('users',['email','firstname','lastname'],['userid'=>$log['userid']]);
        $avatar = getGravatar($user['email']);

        echo'
        <a href="#" onclick="openLogItem('.$functionID.')">
            <div class="log-user"><div class="log-user__avatar" style="background-image:url('.$avatar.')"></div><span>'.$user['firstname'].'</span></div>
            <div class="log-description">'.$log['description'].'</div>
            <div class="log-time">'.$time.'</div>
        </a>
        ';
    }
}

function renderTeamUser($teamUser){
	$avatar = getGravatar($teamUser['email']);

	switch($teamUser['authority']){
		case 0:
		$buttons = '
		<button type="button" onclick="" class="dropdown-item"><i class="fas fa-user fa-fw"></i>Make User</button>
		<button type="button" onclick="removeTeam('."'".$teamUser['userid']."'".')" class="dropdown-item"><span class="text--red"><i class="fas fa-trash fa-fw"></i>Remove</span></button>
		';
		break;
		case 1:
		$buttons = '
		<button type="button" onclick="" class="dropdown-item"><i class="fas fa-user-shield fa-fw"></i>Make Admin</button>
		<button type="button" onclick="removeTeam('."'".$teamUser['userid']."'".')" class="dropdown-item"><span class="text--red"><i class="fas fa-trash fa-fw"></i>Remove</span></button>
		';
		break;
	}

	echo'
	<div class="object-table__row object-table__row--controls">
		<div class="object-table__row__body object-table__row__body--users">
			<div class="object-table-users__avatar" style="background-image:url('.$avatar.');?>)">
			</div>
			<div class="object-table-users__name">
				'.$teamUser['firstname'].' '.$teamUser['lastname'].'
			</div>
			<div class="object-table-users__email">
				'.$teamUser['email'].'
			</div>
		</div>
		<button class="object-table__row__controls" type="button" data-toggle="dropdown"><i class="fas fa-ellipsis-v fa-fw"></i></button>
		<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			'.$buttons.'
		</div>
	</div>
	';
}
function renderTeamInvite($invite){
	$avatar = getGravatar($invite['email']);
	echo'
	<div class="object-table__row object-table__row--controls">
		<div class="object-table__row__body object-table__row__body--usersInvite">
			<div class="object-table-users__avatar" style="background-image:url('.$avatar.');?>)">
			</div>
			<div class="object-table-users__name">
				'.$invite['email'].'
			</div>
		</div>
		<button class="object-table__row__controls" type="button" data-toggle="dropdown"><i class="fas fa-ellipsis-v fa-fw"></i></button>
		<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
			<a class="dropdown-item" href="#"><span class="text--red"><i class="fas fa-times fa-fw"></i>Cancel</span></a>
		</div>
	</div>
	';
}
?>
