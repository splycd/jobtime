<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
checkLogin();

$count = $database->count('timers',[
    'userid'=>$_SESSION['userid']
]);

if($count < 1){
    $response->status = 'clear';
    echo json_encode($response);
    exit;
}else{
    $timer = $database->get('timers','*',[
        'userid'=>$_SESSION['userid']
    ]);
    
    $response->status = 'active';
    $response->startTime = date("Y/m/d H:i:s",strtotime($timer['start']));
    echo json_encode($response);
    exit;
}

?>