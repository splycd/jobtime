<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
checkLogin();

if(checkReadOnly()){
	$response->status = 'error';
    $response->errorMessage = 'JobTime is currently in Read Only Mode.';
    echo json_encode($response);
    exit;
}

$count = $database->count('timers',[
    'userid'=>$_SESSION['userid']
]);

if($count > 0){
    $response->status = 'error';
    $response->errorMessage = 'Timer for this user already exists.';
    echo json_encode($response);
    exit;
}else{

    $currentTime = date("Y/m/d H:i:s");

    $database->insert('timers',[
        'userid'=>$_SESSION['userid'],
        'start'=>$currentTime
    ]);

    $response->status = 'success';
    $response->currentTime = date("Y/m/d H:i:s");
    echo json_encode($response);
    exit;
}

?>
