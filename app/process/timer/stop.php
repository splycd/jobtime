<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
checkLogin();

if(checkReadOnly()){
	$response->status = 'error';
    $response->errorMessage = 'JobTime is currently in Read Only Mode.';
    echo json_encode($response);
    exit;
}

$teamSettings = teamSettings();

$count = $database->count('timers',[
    'userid'=>$_SESSION['userid']
]);

if($count < 1){
    $response->status = 'error';
    $response->errorMessage = 'No timer for this user exists.';
    echo json_encode($response);
    exit;
}else{

    $stopTime = date("Y/m/d H:i:s");
    $startTime = $database->get('timers','start',[
        'userid'=>$_SESSION['userid']
    ]);

    $seconds = strtotime($stopTime) - strtotime($startTime);

    $database->delete('timers',[
	   'userid'=>$_SESSION['userid']
    ]);

    $response->startTime = $startTime;
    $response->stopTime = $stopTime;
    $response->seconds = $seconds;
    $response->step = $teamSettings['timerStep'];

    $response->status = 'success';
    $response->currentTime = date("Y/m/d H:i:s");
    echo json_encode($response);
    exit;
}

?>
