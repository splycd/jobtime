<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';

if(!checkAuthority(0)){
	$response->status = 'error';
    $response->message = 'You do not have the required authority.';
    echo json_encode($response);
    exit;
}

$customerid = $database->get('teams','customerid',['teamid'=>$_SESSION['teamid']]);
if(empty($customerid)){
	$response->status = 'error';
    $response->message = 'An error occured. Please contact support for assistance.';
    echo json_encode($response);
    exit;
}

$billingPortal = $stripe->billingPortal->sessions->create([
  	'customer' => $customerid,
  	'return_url' => $conf['general.systemURL'].'settings',
]);

$response->status = 'success';
$response->url = $billingPortal->url;
echo json_encode($response);
exit;
?>
