<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
checkLogin();

if(checkReadOnly()){
	$response->status = 'error';
    $response->errorMessage = 'JobTime is currently in Read Only Mode.';
    echo json_encode($response);
    exit;
}

$clientName = $_POST['clientName'];
$clientid = generateRandomString(30);

if(empty($clientName)){
    $response->status = 'error';
    $response->errorMessage = 'Missing required information.';
    echo json_encode($response);
    exit;
}
$database->insert('clients',[
    'clientid'=>$clientid,
    'teamid'=>$_SESSION['teamid'],
    'name'=>$clientName
]);

$response->status = 'success';
$response->successRedirect = '/clients/'.$clientid;
echo json_encode($response);
exit;
?>
