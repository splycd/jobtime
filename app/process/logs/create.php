<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
checkLogin();

if(checkReadOnly()){
	$response->status = 'error';
    $response->errorMessage = 'JobTime is currently in Read Only Mode.';
    echo json_encode($response);
    exit;
}

$jobid = $_POST['jobid'];
$clientid = $database->get('jobs','clientid',[
    'jobid'=>$jobid
]);
$hours = $_POST['timeHours'];
$mins = $_POST['timeMins'];
$description = htmlspecialchars($_POST['description'], ENT_QUOTES);

if(empty($jobid) || empty($clientid)){
    $response->status = 'error';
    $response->errorMessage = 'Missing required information.';
    echo json_encode($response);
    exit;
}

if($hours == 0 && $mins == 0){
    $response->status = 'error';
    $response->errorMessage = "You can't enter a log with no time.";
    echo json_encode($response);
    exit;
}

$seconds = ($hours * 60 * 60) + ($mins * 60);


$database->insert('logs',[
    'jobid'=>$jobid,
    'clientid'=>$clientid,
    'userid'=>$_SESSION['userid'],
    'teamid'=>$_SESSION['teamid'],
    'seconds'=>$seconds,
    'description'=>$description,
]);

$database->update('jobs',[
	'dateUpdated'=>date("Y-m-d H:i:s"),
],[
	'jobid'=>$jobid,
]);

$response->status = 'success';
$response->successCallback = 'reloadPage';
echo json_encode($response);
exit;
?>
