<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
checkLogin();

$logid = $_POST['logid'];

if(empty($logid)){
    $response->status = 'error';
    $response->message = 'Missing required information.';
    echo json_encode($response);
    exit;
}

$count = $database->count('logs',[
	'id'=>$logid
]);

if($count < 1){
	$response->status = 'error';
    $response->message = 'Requested time log does not exist.';
    echo json_encode($response);
    exit;
}else{
	$logData = $database->get('logs',[
		'[>]clients'=>'clientid',
		'[>]jobs'=>'jobid'
	],[
		'logs.seconds [Int]',
		'logs.description',
		'logs.dateCreated',
		'logs.teamid',
		'jobs.name (jobName)',
		'clients.name (clientName)'
	],[
		'logs.id'=>$logid
	]);

	if($logData['teamid'] != $_SESSION['teamid']){
		$response->status = 'error';
	    $response->message = 'Requested time log does not belong to your team.';
	    echo json_encode($response);
	    exit;
	}else{
		$response->status = 'success';
	    $response->logData = $logData;
	    echo json_encode($response);
	    exit;
	}
}
?>
