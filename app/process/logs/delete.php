<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
checkLogin();

if(checkReadOnly()){
	$response->status = 'error';
    $response->errorMessage = 'JobTime is currently in Read Only Mode.';
    echo json_encode($response);
    exit;
}

$logid = $_POST['logid'];

if(empty($logid)){
    $response->status = 'error';
    $response->message = 'Missing required information.';
    echo json_encode($response);
    exit;
}

$count = $database->count('logs',[
	'id'=>$logid
]);

if($count < 1){
	$response->status = 'error';
    $response->message = 'Requested time log does not exist.';
    echo json_encode($response);
    exit;
}else{
	$logData = $database->get('logs','*',[
		'id'=>$logid
	]);

	if($logData['teamid'] != $_SESSION['teamid']){
		$response->status = 'error';
	    $response->message = 'Requested time log does not belong to your team.';
	    echo json_encode($response);
	    exit;
	}else{

		$database->delete('logs',[
			'id'=>$logid
		]);

		$_SESSION['notification'] = array(
		        'type' => 'toast',
		        'style' => 'warning',
				'title' => 'Log Deleted',
		    	'content' => 'The time log has been deleted.',
		);

		$response->status = 'success';
	    echo json_encode($response);
	    exit;
	}
}
?>
