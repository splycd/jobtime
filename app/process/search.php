<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
checkLogin();

header('Content-Type: application/json');

$query = $_POST['query'];
//$query = 'web';

if(strlen($query) < 3){
	$response->status = 'error';
    $response->errorMessage = 'Query not long enough.';
    echo json_encode($response);
    exit;
}else{
	$jobClients = $database->select('jobs','clientid',[
		'AND'=>[
			'teamid'=>$_SESSION['teamid'],
			'name[~]'=>$query,
		],
	]);
	$archiveClients = $database->select('jobArchives','clientid',[
		'AND'=>[
			'teamid'=>$_SESSION['teamid'],
			'name[~]'=>$query,
		],
	]);

	$jobArchiveClients = array_unique(array_merge($jobClients, $archiveClients));

	$clientNames = $database->select('clients','clientid',[
		'AND'=>[
			'teamid'=>$_SESSION['teamid'],
			'name[~]'=>$query,
		],
	]);

	$clientids = array_unique(array_merge($jobArchiveClients, $clientNames));

	//var_dump($clientids);

	$results = array();

	foreach ($clientids as $clientid){
		$client = $database->get('clients','*',[
			'clientid'=>$clientid,
		]);

		$jobCount = $database->count('jobs',[
			'clientid'=>$clientid,
		]);
		$archiveCount = $database->count('jobArchives',[
			'clientid'=>$clientid,
		]);

		$jobs = $database->select('jobs',[
			'jobid',
			'name',
		],[
			'AND'=>[
				'teamid'=>$_SESSION['teamid'],
				'clientid'=>$clientid,
				'name[~]'=>$query,
			],
		]);
		$archive = $database->select('jobArchives',[
			'jobid',
			'name',
		],[
			'AND'=>[
				'teamid'=>$_SESSION['teamid'],
				'clientid'=>$clientid,
				'name[~]'=>$query,
			],
		]);

		$clientArray = array(
			'clientid'=>$clientid,
			'name'=>$client['name'],
			'totalJobs'=>$jobCount,
			'totalArchive'=>$archiveCount,
			'searchJobsCount'=>count($jobs),
			'searchArchiveCount'=>count($archive),
			'searchJobs'=>$jobs,
			'searchArchive'=>$archive,
		);

		array_push($results, $clientArray);
	}

	$response->status = 'success';
    $response->results = $results;
    echo json_encode($response);
    exit;
}
?>
