<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';

$email = $_POST['email'];
$password = $_POST['password'];

/*
if(!empty($_SESSION['loginUrl'])){
    $loginUrl = $_SESSION['loginUrl'];
}else{
    $loginUrl = '/';
}
*/

if(empty($email) || empty($password)){
    $errorFields = array();
    if(empty($email)){
        array_push($errorFields, 'email');
    }
    if(empty($password)){
        array_push($errorFields, 'password');
    }

    $response->status = 'error';
    $response->errorMessage = 'Missing required information.';
    $response->errorFields = $errorFields;
    echo json_encode($response);
    exit;
}

$emailCount = $database->count('users',[
    'email'=>$email
]);
if($emailCount < 1){
    $response->status = 'error';
    $response->errorMessage = 'Email Address or Password is incorrect. (E:1)';
    $response->errorFields = array('email','password');
    echo json_encode($response);
    exit;
}else{
    $passwordHash = $database->get('users','password',[
        'email'=>$email
    ]);

    if(password_verify($password,$passwordHash)){
		/*
        $_SESSION['userid'] = $database->get('users','userid',[
            'email'=>$email
        ]);
        setcookie('userid', $_SESSION['userid'], time() + (86400 * 365), "/");

        $teamCount = $database->count('teamAssoc',[
            'userid'=>$_SESSION['userid']
        ]);

        if($teamCount < 1){

        }elseif($teamCount == 1){
            $_SESSION['teamid'] = $database->get('teamAssoc','teamid',['userid'=>$_SESSION['userid']]);
            setcookie('teamid', $_SESSION['teamid'], time() + (86400 * 365), "/");
        }
        */

		$_SESSION['userid'] = $database->get('users','userid',[
            'email'=>$email
        ]);
		$_SESSION['teamid'] = $database->get('users','teamid',[
            'email'=>$email
        ]);

		$loginToken = generateRandomString(60);
		$database->insert('loginToken',[
		    'userid'=>$_SESSION['userid'],
		    'token'=>$loginToken
		]);
		setcookie('token', $loginToken, time() + (86400 * 365), "/");


        $response->status = 'success';
        $response->successRedirect = '/';
        echo json_encode($response);
        exit;
    }else{
        $response->status = 'error';
        $response->errorMessage = 'Email Address or Password is incorrect. (E:2)';
        $response->errorFields = array('email','password');

        echo json_encode($response);
        exit;
    }
}
?>
