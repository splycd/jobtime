<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';

if(checkReadOnly()){
	$response->status = 'error';
    $response->errorMessage = 'JobTime is currently in Read Only Mode.';
    echo json_encode($response);
    exit;
}

$teamname = $_POST['teamname'];
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$email = $_POST['email'];
$password1 = $_POST['password1'];
$password2 = $_POST['password2'];
$userid = generateRandomString(30);
$teamid = generateRandomString(30);

if(empty($teamname) || empty($firstname) || empty($lastname) || empty($email) || empty($password1) || empty($password2)){
    $errorFields = array();
	if(empty($teamname)){
        array_push($errorFields, 'teamname');
    }
    if(empty($firstname)){
        array_push($errorFields, 'firstname');
    }
    if(empty($lastname)){
        array_push($errorFields, 'lastname');
    }
    if(empty($email)){
        array_push($errorFields, 'email');
    }
    if(empty($password1)){
        array_push($errorFields, 'password1');
    }
    if(empty($password2)){
        array_push($errorFields, 'password2');
    }

    $response->status = 'error';
    $response->errorMessage = 'Missing required information.';
    $response->errorFields = $errorFields;
    echo json_encode($response);
    exit;
}

if($password1 != $password2){
    $response->status = 'error';
    $response->errorMessage = 'Password Missmatch.';
    $response->errorFields = array('password','password2');
    echo json_encode($response);
    exit;
}

$emailCount = $database->count('users',[
    'email'=>$email
]);
if($emailCount > 0){
    $response->status = 'error';
    $response->errorMessage = 'Email already in use.';
    $response->errorFields = array('email');
    echo json_encode($response);
    exit;
}

$passwordHash = password_hash($password1, PASSWORD_DEFAULT);


$defaultSettings = array(
    'timerStep' => 15
);
$defaultSettingsJson = json_encode($defaultSettings);

$stripeCustomer = $stripe->customers->create([
	'email' => $email,
	'name' => $teamname,
]);

$database->insert('teams',[
    'teamid'=>$teamid,
    'name'=>$teamname,
	'customerid'=>$stripeCustomer->id,
    'settings'=>$defaultSettingsJson,
]);

$database->insert('users',[
    'userid'=>$userid,
    'teamid'=>$teamid,
    'email'=>$email,
    'firstname'=>$firstname,
    'lastname'=>$lastname,
    'password'=>$passwordHash,
	'authority'=>0
]);

$token = generateRandomString(60);
$database->insert('userVerify',[
    'userid'=>$userid,
    'token'=>$token
]);

$loginToken = generateRandomString(60);
$database->insert('loginToken',[
    'userid'=>$userid,
    'token'=>$loginToken
]);

$_SESSION['userid'] = $userid;
setcookie('token', $loginToken, time() + (86400 * 365), "/");

$verifyUrl = $conf['general.systemURL'].'verify/'.$token;

try {
    //Recipients
    $mail->setFrom('no-reply@jobtime.app', 'JobTime');
    $mail->addAddress($email, $name);     // Add a recipient

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Your New JobTime Account';
    $mail->Body    = '
    <h1>Welcome to JobTime!</h1>
    <p>Please verify your email address by clicking on the following link, or copying it into your browser:</p>
    <p><a href="'.$verifyUrl.'">'.$verifyUrl.'</a></p>
    ';
    $mail->AltBody = 'Welcome to JobTime! Please verify your email address by copying the following link into your browser: '.$verifyUrl.'.';

    $mail->send();
    $response->status = 'success';
    $response->successRedirect = '/';
    echo json_encode($response);
    exit;
} catch (Exception $e) {
    //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    $response->status = 'error';
    $response->errorMessage = 'An error has occured: E001.';
    echo json_encode($response);
    exit;
}
?>
