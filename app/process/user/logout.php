<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';

$database->delete('loginToken',['token'=>$_COOKIE['token']]);

$_SESSION['userid'] = NULL;
$_SESSION['teamid'] = NULL;
setcookie('token', NULL, time() - (86400 * 365), "/");


$_SESSION['notification'] = array(
        'type' => 'toast',
        'style' => 'warning',
        'title' => 'See you later!',
        'content' => 'You have now been signed out.',
    );
header('Location: /login');
exit;
?>
