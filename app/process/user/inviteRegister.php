<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';

if(checkReadOnly()){
	$response->status = 'error';
    $response->errorMessage = 'JobTime is currently in Read Only Mode.';
    echo json_encode($response);
    exit;
}

$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$email = $_POST['email'];
$password1 = $_POST['password1'];
$password2 = $_POST['password2'];
$userid = generateRandomString(30);

$teamid = $database->get('userInvite','teamid',['token'=>$_POST['token']]);

if(empty($firstname) || empty($lastname) || empty($email) || empty($password1) || empty($password2)){
    $errorFields = array();
    if(empty($firstname)){
        array_push($errorFields, 'firstname');
    }
    if(empty($lastname)){
        array_push($errorFields, 'lastname');
    }
    if(empty($email)){
        array_push($errorFields, 'email');
    }
    if(empty($password1)){
        array_push($errorFields, 'password1');
    }
    if(empty($password2)){
        array_push($errorFields, 'password2');
    }

    $response->status = 'error';
    $response->errorMessage = 'Missing required information.';
    $response->errorFields = $errorFields;
    echo json_encode($response);
    exit;
}

if($password1 != $password2){
    $response->status = 'error';
    $response->errorMessage = 'Password Missmatch.';
    $response->errorFields = array('password','password2');
    echo json_encode($response);
    exit;
}

$emailCount = $database->count('users',[
    'email'=>$email
]);
if($emailCount > 0){
    $response->status = 'error';
    $response->errorMessage = 'Email already in use.';
    $response->errorFields = array('email');
    echo json_encode($response);
    exit;
}

$passwordHash = password_hash($password1, PASSWORD_DEFAULT);

$database->insert('users',[
    'userid'=>$userid,
    'teamid'=>$teamid,
    'email'=>$email,
    'firstname'=>$firstname,
    'lastname'=>$lastname,
    'password'=>$passwordHash,
	'authority'=>1
]);

$database->delete('userInvite',['token'=>$_POST['token']]);


$loginToken = generateRandomString(60);
$database->insert('loginToken',[
    'userid'=>$userid,
    'token'=>$loginToken
]);

$_SESSION['userid'] = $userid;
setcookie('token', $loginToken, time() + (86400 * 365), "/");


$response->status = 'success';
$response->successRedirect = '/';
echo json_encode($response);
?>
