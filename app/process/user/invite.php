<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
checkLogin();

if(checkReadOnly()){
	$response->status = 'error';
    $response->errorMessage = 'JobTime is currently in Read Only Mode.';
    echo json_encode($response);
    exit;
}

if(!checkAuthority(0)){
	$response->status = 'error';
    $response->errorMessage = 'You do not have the required authority.';
    echo json_encode($response);
    exit;
}

$team = $database->get('teams','*',['teamid'=>$_SESSION['teamid']]);

$inviteEmail = $_POST['email'];

$memberCount = $database->count('users',['teamid'=>$_SESSION['teamid']]);
if($memberCount >= 5 && checkSubscription() == 'free'){
	$response->status = 'error';
    $response->errorMessage = "You've reached your limit of 5 Team Members. Upgrade to JobTime Pro for unlimited Team Members.";
    echo json_encode($response);
    exit;
}

if(empty($inviteEmail)){
    $response->status = 'error';
    $response->errorMessage = 'Please enter an email address to invite.';
    $response->errorFields = array('email');
    echo json_encode($response);
    exit;
}

$currentEmail = $database->get('users','email',['userid'=>$_SESSION['userid']]);

if($inviteEmail == $currentEmail){
    $response->status = 'error';
    $response->errorMessage = "You can't invite yourself!";
    $response->errorFields = array('email');
    echo json_encode($response);
    exit;
}

$teamCount = $database->count('users',[
	'AND'=>[
		'teamid'=>$_SESSION['teamid'],
		'email'=>$inviteEmail
	]
]);
if($teamCount > 0){
    $response->status = 'error';
    $response->errorMessage = "This person is already in your team.";
    $response->errorFields = array('email');
    echo json_encode($response);
    exit;
}

$userCount = $database->count('users',[
	'email'=>$inviteEmail
]);
if($userCount > 0){
    $response->status = 'error';
    $response->errorMessage = "This person is a member of another team.";
    $response->errorFields = array('email');
    echo json_encode($response);
    exit;
}

$token = generateRandomString(60);
$database->insert('userInvite',[
	'teamid'=>$_SESSION['teamid'],
    'email'=>$inviteEmail,
    'token'=>$token
]);

$inviteUrl = $conf['general.systemURL'].'invite/'.$token;

try {
    //Recipients
    $mail->setFrom('no-reply@jobtime.app', 'JobTime');
    $mail->addAddress($inviteEmail);     // Add a recipient

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Join '.$team['name'].' on JobTime';
    $mail->Body    = '
    <h1>Join '.$team['name'].' on JobTime!</h1>
    <p>You have been invited to join '.$team['name'].' on JobTime. Use the following link to accept your invitation.</p>
    <p><a href="'.$inviteUrl.'">'.$inviteUrl.'</a></p>
    ';
    $mail->AltBody = 'You have been invited to join '.$team['name'].' on JobTime. Use the following link to accept your invitation: '.$inviteUrl.'.';

    $mail->send();
	$response->status = 'success';
	$_SESSION['notification'] = array(
	    'type' => 'toast',
	    'style' => 'success',
	    'title' => 'Invited!',
	    'content' => $inviteEmail.' has been invited to your team.',
	);
    $response->successRedirect = '/settings';
    echo json_encode($response);
    exit;
} catch (Exception $e) {
    //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    $response->status = 'error';
    $response->errorMessage = 'An error has occured: E001.';
    echo json_encode($response);
    exit;
}
?>
