<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
checkLogin();

if(checkReadOnly()){
	$response->status = 'error';
    $response->message = 'JobTime is currently in Read Only Mode.';
    echo json_encode($response);
    exit;
}
if(!checkAuthority(0)){
	$response->status = 'error';
    $response->message = 'You do not have the required authority.';
    echo json_encode($response);
    exit;
}

$jobid = $_POST['jobid'];

if(empty($jobid)){
    $response->status = 'error';
    $response->message = 'Missing required information.';
    echo json_encode($response);
    exit;
}

$count = $database->count('jobs',[
	'jobid'=>$jobid
]);

if($count < 1){
	$response->status = 'error';
    $response->message = 'Requested Job does not exist.';
    echo json_encode($response);
    exit;
}else{
	$jobData = $database->get('jobs','*',[
		'jobid'=>$jobid
	]);

	if($jobData['teamid'] != $_SESSION['teamid']){
		$response->status = 'error';
	    $response->message = 'Requested Job does not belong to your team.';
	    echo json_encode($response);
	    exit;
	}else{

		$totalTime = 0;

		$timeLogs = $database->select('logs','*',[
			'jobid'=>$jobid
		]);

		$timeLogsArray = array();

		foreach($timeLogs as $log){
			$totalTime = $totalTime + $log['seconds'];

			$logData = array(
				'userid'=>$log['userid'],
				'seconds'=>$log['seconds'],
				'description'=>$log['description'],
				'dateCreated'=>$log['dateCreated']
			);

			array_push($timeLogsArray,$logData);
		}

		$jobName = $jobData['name'];
		$clientid = $jobData['clientid'];
		$jobData = array(
			'dateCreated'=>$jobData['dateCreated'],
			'budget'=>$jobData['budget'],
			'totalTime'=>$totalTime,
			'timeLogs'=>$timeLogsArray
		);

		$database->insert('jobArchives',[
			'jobid'=>$jobid,
			'clientid'=>$clientid,
			'teamid'=>$_SESSION['teamid'],
			'name'=>$jobName,
			'data'=>json_encode($jobData)
		]);

		$database->delete('jobs',['jobid'=>$jobid]);

		$response->status = 'success';
	    echo json_encode($response);
	    exit;
	}
}
