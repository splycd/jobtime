<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
checkLogin();

if(checkReadOnly()){
	$response->status = 'error';
    $response->errorMessage = 'JobTime is currently in Read Only Mode.';
    echo json_encode($response);
    exit;
}

$jobName = $_POST['jobName'];
$jobid = generateRandomString(30);
$clientid = $_POST['clientid'];
$budget = $_POST['budget'];

if(empty($jobName) || empty($clientid)){
    $response->status = 'error';
    $response->errorMessage = 'Missing required information.';
    echo json_encode($response);
    exit;
}

if($budget < 1){
    $budget = null;
}

$database->insert('jobs',[
    'jobid'=>$jobid,
    'clientid'=>$clientid,
    'teamid'=>$_SESSION['teamid'],
    'name'=>$jobName,
    'budget'=>$budget
]);

$response->status = 'success';
$response->successRedirect = '/jobs/'.$jobid;
echo json_encode($response);
exit;
?>
