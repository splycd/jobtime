<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
$PAGE_TITLE = 'Clients';
$PAGE_SLUG = 'clients';
checkLogin();
checkTeam();
$teamSettings = teamSettings();

$user = $database->get('users','*',[
    'userid'=>$_SESSION['userid']
]);
$team = $database->get('teams','*',[
    'teamid'=>$_SESSION['teamid']
]);
?>

<!doctype html>
<html lang="en">
    <?php getInclude('head.php');?>
    <body>
        <div class="structure">
            <?php getInclude('sidebar.php');?>
            <div class="structure__main">
                <?php getInclude('timer.php');?>
                <?php getInclude('top-bar.php');?>

				<section>
					<div class="container">
						<div class="heading-action">
                            <h1 class="m-0">Clients</h1>
                            <button class="button button--red" data-toggle="modal" data-target="#createClientModal">Create Client</button>
                        </div>
						<div class="object-table">
							<?php
                            $clients = $database->select('clients','*',[
                                'teamid'=>$_SESSION['teamid'],
								'ORDER'=>['name'=>'ASC'],
                            ]);
                            foreach($clients as $client){
                                renderClientTable($client);
                            }
                            ?>
						</div>
					</div>
				</section>
            </div>
        </div>

        <div class="modal" id="createClientModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Client</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="process-form" method="post" action="/process/clients/create.php">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Client Name</label>
                                        <input type="text" class="input-text" id="clientName" name="clientName" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="button button--red button--fw">Create Client</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php getInclude('scripts.php');?>
    </body>
</html>
