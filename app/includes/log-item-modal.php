<div class="modal" id="logItemModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Time Log</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="process-form">
					<div class="row">
						<div class="col-12">
							<div class="form-group">
								<label>Time (h:m)<span><?php echo $timeLabel;?></span></label>
								<div class="time-input time-input--readonly">
									<input type="number" id="logItemModal-hours" name="timeHours" value="0" readonly>
									<input type="number" id="logItemModal-mins" name="timeMins" value="0" readonly>
								</div>
							</div>
							<div class="form-group">
								<label>Job</label>
								<input type="text" class="input-text" id="logItemModal-job" readonly value="">
							</div>
							<div class="form-group">
								<label>Description</label>
								<input type="text" class="input-text" id="logItemModal-description" readonly value="">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 text-right">
							<button type="button" class="button button--transparent" data-dismiss="modal">Cancel</button>
							<button type="button" id="delete-log-button" class="button button--red" data-logid="">Delete</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
