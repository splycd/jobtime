<div class="structure__sidebar">
    <a href="/" class="branding">
        <img class="logo-dark" src="/assets/logo-dark.svg">
        <img class="logo-light" src="/assets/logo-light.svg">
    </a>
    <nav class="sidebar-nav">
        <ul>
            <li><a href="/"<?php if($GLOBALS['PAGE_SLUG'] == 'index'){echo' class="active"';}?>><i class="fas fa-tachometer-alt fa-fw"></i> Dashboard</a></li>
            <li><a href="/jobs"<?php if($GLOBALS['PAGE_SLUG'] == 'jobs'){echo' class="active"';}?>><i class="fas fa-briefcase fa-fw"></i> Jobs</a></li>
            <li><a href="/clients"<?php if($GLOBALS['PAGE_SLUG'] == 'clients'){echo' class="active"';}?>><i class="fas fa-user-friends fa-fw"></i> Clients</a></li>
			<?php if(checkAuthority(0)){?>
            <li><a href="/settings"<?php if($GLOBALS['PAGE_SLUG'] == 'settings'){echo' class="active"';}?>><i class="fas fa-cog fa-fw"></i> Team Settings</a></li>
			<?php }?>
        </ul>
    </nav>
    <a class="sidebar-profile" href="/account">
        <div class="sidebar-profile__avatar" style="background-image: url(<?php echo getGravatar($GLOBALS['user']['email']);?>)"></div>
        <div class="sidebar-profile__body">
            <span class="sidebar-profile__team">
				<?php if(checkSubscription() == 'pro'){echo'<span class="sidebar-profile__team-badge">PRO</span> ';}?>
				<?php echo $GLOBALS['team']['name'];?>
			</span>
            <span class="sidebar-profile__name"><?php echo $GLOBALS['user']['firstname'];?> <?php echo $GLOBALS['user']['lastname'];?></span>
        </div>
    </a>
    <ul class="sidebar-links">
        <li><a href="#">Terms</a></li>
        <li><a href="#">Privacy</a></li>
    </ul>
</div>
<div class="structure__sidebar-narrow">
    <a href="/" class="branding">
        <img src="/assets/logo-icon.svg">
    </a>
    <nav class="sidebar-nav">
        <ul>
            <li><a href="/"<?php if($GLOBALS['PAGE_SLUG'] == 'index'){echo' class="active"';}?>><i class="fas fa-tachometer-alt fa-fw"></i></a></li>
            <li><a href="/jobs"<?php if($GLOBALS['PAGE_SLUG'] == 'jobs'){echo' class="active"';}?>><i class="fas fa-briefcase fa-fw"></i></a></li>
            <li><a href="/clients"<?php if($GLOBALS['PAGE_SLUG'] == 'clients'){echo' class="active"';}?>><i class="fas fa-user-friends fa-fw"></i></a></li>
			<?php if(checkAuthority(0)){?>
            <li><a href="/settings"<?php if($GLOBALS['PAGE_SLUG'] == 'settings'){echo' class="active"';}?>><i class="fas fa-cog fa-fw"></i></a></li>
			<?php }?>
        </ul>
    </nav>
    <a class="sidebar-profile" href="/account">
        <div class="sidebar-profile__avatar" style="background-image: url(<?php echo getGravatar($GLOBALS['user']['email']);?>)"></div>
    </a>
    <ul class="sidebar-links">
        <li><a href="#">Terms</a></li>
        <li><a href="#">Privacy</a></li>
    </ul>
</div>

<nav class="mobile-nav">
    <a class="mobile-nav__item<?php if($GLOBALS['PAGE_SLUG'] == 'index'){echo' active';}?>" href="/">
        <i class="fas fa-tachometer-alt"></i>
        <span>Dashboard</span>
    </a>
    <a class="mobile-nav__item<?php if($GLOBALS['PAGE_SLUG'] == 'jobs'){echo' active';}?>" href="/jobs">
        <i class="fas fa-briefcase"></i>
        <span>Jobs</span>
    </a>
    <a class="mobile-nav__item<?php if($GLOBALS['PAGE_SLUG'] == 'clients'){echo' active';}?>" href="/clients">
        <i class="fas fa-user-friends"></i>
        <span>Clients</span>
    </a>
	<?php if(checkAuthority(0)){?>
	<a class="mobile-nav__item<?php if($GLOBALS['PAGE_SLUG'] == 'settings'){echo' active';}?>" href="/settings">
	    <i class="fas fa-cog"></i>
	    <span>Settings</span>
	</a>
	<?php }?>
	<a class="mobile-nav__item" href="/account">
        <div class="mobile-nav__item__avatar" style="background-image: url(<?php echo getGravatar($GLOBALS['user']['email']);?>)"></div>
        <span><?php echo $GLOBALS['user']['firstname'];?></span>
    </a>
</nav>
