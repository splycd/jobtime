<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/js.cookie.min.js"></script>

<script src="/js/pushy.min.js"></script>
<script src="/js/fancybox.min.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script src="/js/toastr.min.js"></script>
<script src="/js/easytimer.min.js"></script>

<script src="/js/deviceDetect.js?v=<?php echo $GLOBALS['CacheVersion'];?>"></script>
<script src="/js/search.js?v=<?php echo $GLOBALS['CacheVersion'];?>"></script>
<script src="/js/scripts.js?v=<?php echo $GLOBALS['CacheVersion'];?>"></script>

<?php if(!empty($_SESSION['notification'])){
    if($_SESSION['notification']['type'] == 'modal'){
        echo'
        <script>swal ( "'.$_SESSION['notification']['title'].'" ,  "'.$_SESSION['notification']['content'].'" ,  "'.$_SESSION['notification']['style'].'" );</script>
        ';
    }elseif($_SESSION['notification']['type'] == 'toast'){
		echo'
		<script>toastr["'.$_SESSION['notification']['style'].'"]("'.$_SESSION['notification']['content'].'", "'.$_SESSION['notification']['title'].'")</script>
		';
    }
    $_SESSION['notification'] = '';
}?>
