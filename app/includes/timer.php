<?php
$timerStep = $GLOBALS['teamSettings']['timerStep'];
if($timerStep == 1){
    $timeLabel = 'Rounded to 1 minute';
}else{
    $timeLabel = 'Rounded to '.$timerStep.' minutes';
}
?>
<div class="timer-bar">
    <div class="container">
        <div class="timer-box">
            <div>Timer Running:<span id="timer-time">--h --m --s</span></div>
            <button class="button button--white" onclick="stopTimer()">Stop Timer</button>
        </div>
    </div>
</div>
<div class="modal" id="logTimeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Log Time</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="process-form" method="post" action="/process/logs/create.php">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Time (h:m)<span><?php echo $timeLabel;?></span></label>
                                <div class="time-input">
                                    <input type="number" id="time-input-hours" name="timeHours" value="0" min="0" max="999" required>
									<div class="time-input__controls">
                                        <button type="button" onclick="hoursStepUp(<?php echo $timerStep;?>)"><i class="fas fa-chevron-up"></i></button>
                                        <button type="button" onclick="hoursStepDown(<?php echo $timerStep;?>)"><i class="fas fa-chevron-down"></i></button>
                                    </div>
                                    <input type="number" id="time-input-mins" name="timeMins" value="0" min="0" max="60" step="<?php echo $timerStep;?>" required>
                                    <div class="time-input__controls">
                                        <button type="button" onclick="minsStepUp(<?php echo $timerStep;?>)"><i class="fas fa-chevron-up"></i></button>
                                        <button type="button" onclick="minsStepDown(<?php echo $timerStep;?>)"><i class="fas fa-chevron-down"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Job</label>
                                <select class="input-dropdown" name="jobid" required>
                                    <option value="">Please Select</option>
                                    <?php
                                    $jobs = $GLOBALS['database']->select('jobs',[
                                        '[>]clients'=>'clientid'
                                    ],[
                                        'jobs.name(jobname)',
                                        'jobs.jobid',
                                        'jobs.budget [Int]',
                                        'clients.name(clientname)',
                                    ],[
                                        'jobs.teamid'=>$_SESSION['teamid'],
                                        'ORDER'=>[
                                            'dateUpdated'=>'DESC'
                                        ]
                                    ]);
                                    foreach($jobs as $job){
                                        echo'<option value="'.$job['jobid'].'">'.$job['jobname'].' ('.$job['clientname'].')</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="input-text" id="description" id="description" name="description" placeholder="Briefly explain what you were doing.">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="button" class="button button--transparent" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="button button--red">Log Time</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
