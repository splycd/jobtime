<div class="search-overlay"></div>
<div class="search-results-container">
	<div class="search-results">
		<div class="search-results__grid">
			<div class="search-results__grid-item">
				<a href="#" class="search-results__client">
					<span>The Honest Hub</span>
					<ul>
						<li>Jobs: 1</li>
						<li>Archived: 0</li>
					</ul>
				</a>
				<div class="search-results__jobs">
					<a href="#">
						<span class="search-results__jobs__title">Title</span>
						<span class="search-results__jobs__type">Active</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="top-bar">
    <div class="container">
        <div class="top-bar__grid">
            <div class="top-bar__search">
                <i class="fas fa-search"></i>
                <input id="search-bar" type="search" placeholder="Search for Clients or Jobs">
				<i class="fas fa-spinner fa-pulse"></i>
            </div>
            <div class="top-bar__buttons">
                <button class="button button--yellow" data-toggle="modal" data-target="#logTimeModal">Log Time</button>
                <button class="button button--red" onclick="startTimer()" id="top-bar-start">Start Timer</button>
                <button class="button button--red" onclick="stopTimer()" id="top-bar-stop">Stop Timer</button>
            </div>
        </div>
    </div>
</div>
<div class="mobile-top-bar">
	<a href="/" class="mobile-top-bar__branding">
		<img class="logo-dark" src="/assets/logo-dark.svg">
        <img class="logo-light" src="/assets/logo-light.svg">
	</a>
</div>
