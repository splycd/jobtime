<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
$PAGE_TITLE = 'Create Account';
$PAGE_SLUG = 'register';
//checkLogin();
//checkTeam();

$wallpaper = getWallpapers();
?>

<!doctype html>
<html lang="en">
    <?php getInclude('head.php');?>
    <body>
        <div class="account-structure">
            <div class="account-structure__main">
                <div class="account-structure__single-nav">
                    <a href="/" class="account-structure__branding">
                        <img class="logo-dark" src="/assets/logo-dark.svg">
                        <img class="logo-light" src="/assets/logo-light.svg">
                    </a>
                </div>
                <!--
                <div class="account-structure__split-nav">
                    <a href="/" class="account-structure__branding">
                        <img class="logo-dark" src="/assets/logo-dark.svg">
                        <img class="logo-light" src="/assets/logo-light.svg">
                    </a>
                    <a href="#" class="account-structure__split-nav__button"><i class="fas fa-chevron-left"></i> Go Back</a>
                </div>
-->
				<form class="process-form" method="post" action="/process/user/register.php">
					<section class="text-center">
	                    <h1>Create a Team</h1>
						<p>If you are planning on joining an existing team, get an Admin of that team to invite you.</p>
	                </section>
					<section>
						<div class="form-group">
							<label>Team Name</label>
							<input class="input-text" type="text" id="teamname" name="teamname" placeholder="E.g. Splycd Digital">
						</div>
					</section>
					<section>
						<div class="form-group">
							<label>First Name</label>
							<input class="input-text" type="text" id="firstname" name="firstname" placeholder="E.g. John">
						</div>
						<div class="form-group">
							<label>Last Name</label>
							<input class="input-text" type="text" id="lastname" name="lastname" placeholder="E.g. Smith">
						</div>
						<div class="form-group">
							<label>Email Address</label>
							<input class="input-text" type="email" id="email" name="email" placeholder="E.g. example@address.com">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input class="input-text" type="password" id="password1" name="password1" placeholder="***********">
						</div>
						<div class="form-group">
							<label>Re-enter Password</label>
							<input class="input-text" type="password" id="password2" name="password2" placeholder="***********">
						</div>
						<div class="form-group text-center">
							<button type="submit" class="button button--red">Create Team</button>
						</div>
						<div class="form-group text-center">
							<a href="/login" class="heading-link">Or Log in <i class="fas fa-chevron-right"></i></a>
						</div>
					</section>
				</form>
            </div>
			<div class="account-structure__background" style="background-image: url(/assets/wallpapers/<?php echo $wallpaper[0];?>)">
                <a target="_blank" href="<?php echo $wallpaper[2];?>">Photo by <?php echo $wallpaper[1];?> on Unsplash</a>
            </div>
        </div>
        <?php getInclude('scripts.php');?>
    </body>
</html>
