/*var cookieDismiss = Cookies.get('cookieDismiss');
if(cookieDismiss == 'true'){
    $( ".cookie-notice" ).hide();
}

function cookieNoticeDismiss(){
    Cookies.set('cookieDismiss', true, { expires: 365 });
    $( ".cookie-notice" ).hide();
}*/

if(window.iphoneX === true){
var originalHeight = window.innerHeight;
var currentHeight = window.innerHeight;
window.addEventListener("resize", function(){
    currentHeight = window.innerHeight;
    if(currentHeight >= originalHeight){
        $(".mobile-nav").addClass("mobile-nav--ios-scroll");
    }
    if(currentHeight == originalHeight){
        $(".mobile-nav").removeClass("mobile-nav--ios-scroll");
    }
});
}

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-center",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

window.onload = function () {
    //$("#loader").fadeOut();
    //countUpFromTime("2020/03/22 19:00:00");
    checkTimer();
}

function reloadPage(){
    location.reload();
}

function secondsToUnit(unit,totalSeconds){
    hours = Math.floor(totalSeconds / 60 / 60);
    mins = Math.floor((totalSeconds % 3600) / 60);
    secs = Math.floor(totalSeconds - ((mins * 60) + (hours * 3600)));

    if(secs >= 30){
        mins ++;
    }

    if(unit == 'hours'){
        return hours;
    }
    if(unit == 'mins'){
        return mins;
    }
}

function countUpFromTime(countFrom) {
    countFrom = new Date(countFrom).getTime();
    var now = new Date(),
        countFrom = new Date(countFrom),
        timeDifference = (now - countFrom);

    //console.log(timeDifference);

    var totalSeconds = Math.floor(timeDifference / 1000);

    //console.log(totalSeconds);

    hours = Math.floor(totalSeconds / 60 / 60);
    mins = Math.floor((totalSeconds % 3600) / 60);
    secs = Math.floor(totalSeconds - ((mins * 60) + (hours * 3600)));
    var readout = hours+'h '+mins+'m '+secs+'s';

    //console.log(secs);

    document.getElementById('timer-time').innerHTML = readout;

    clearTimeout(countUpFromTime.interval);
    countUpFromTime.interval = setTimeout(function(){ countUpFromTime(countFrom); }, 1000);
}

$( document ).on('submit','.process-form',function(event){
    console.log( $( this ).attr('action') );
    console.log( $( this ).serialize() );
    event.preventDefault();

    $( ".input-control" ).removeClass( "input-control--error" );

    //var submitButtonContent = $('.process-form :submit').html();
	var closestModal = $(this).closest('.modal');
	var submitButton = $(this).find(':submit');
    var submitButtonContent = submitButton.html();
    submitButton.html(submitButtonContent + ' <i class="fas fa-spinner fa-pulse button-spinner"></i>');

    console.log("Button: "+submitButtonContent);

    $.ajax({
        type: "POST",
        url: $( this ).attr('action'),
        data: $(this).serialize(),
        dataType: 'json',
        success: function(response){
            console.log(response);
            if(response.status == 'error'){
                submitButton.html(submitButtonContent);
                swal ( "Oops" ,  response.errorMessage ,  "error" );
                if(typeof response.errorFields != "undefined"){
                    var errorFields = response.errorFields;
                    errorFields.forEach(function(fieldID) {
                        console.log(fieldID);
                        $( "#"+fieldID ).addClass( "input-control--error" );
                    });
                }
            }else if(response.status == 'success'){
                submitButton.html(submitButtonContent);
				closestModal.modal('hide');
                if(typeof response.successRedirect !== 'undefined'){
                    window.location.href = response.successRedirect;
                }
                if(typeof response.successCallback !== 'undefined'){
                    console.log('Callback Function: '+response.successCallback);
                    window[response.successCallback](response.successCallbackParams);
                }
                if(typeof response.successMessage !== 'undefined'){
                    swal ( response.successMessage.title ,  response.successMessage.content ,  response.successMessage.style );
                }
            }
			if(typeof response.alert !== 'undefined'){
				swal ( response.alert.title ,  response.alert.content ,  response.alert.style );
			}
        }
    });
});

function startTimer(){
    event.preventDefault();
    //alert('test');
    $.ajax({
        type: "POST",
        url: '/process/timer/start.php',
        data: '',
        dataType: 'json',
        success: function(response){
            console.log(response);
            if(response.status == 'error'){
                swal ( "Oops" ,  response.errorMessage ,  "error" );
            }else if(response.status == 'success'){
                $('body').addClass('timer-bar--active');
                $('#top-bar-start').hide();
                $('#top-bar-stop').show();
                countUpFromTime(response.currentTime);
            }
        }
    });
}

function stopTimer(){
    event.preventDefault();
    //alert('test');
    $.ajax({
        type: "POST",
        url: '/process/timer/stop.php',
        data: '',
        dataType: 'json',
        success: function(response){
            console.log(response);
            if(response.status == 'error'){
                swal ( "Oops" ,  response.errorMessage ,  "error" );
            }else if(response.status == 'success'){
                $('body').removeClass('timer-bar--active');
                $('#top-bar-start').show();
                $('#top-bar-stop').hide();

                var timerStep = parseInt(response.step);
                var hours = secondsToUnit('hours',response.seconds);
                var mins = secondsToUnit('mins',response.seconds);

                mins = (Math.round(mins/timerStep) * timerStep) % 60;

				if(response.seconds > 60 && mins == 0){
					hours++;
				}

                $("#time-input-hours").val(hours);
                $("#time-input-mins").val(mins);

                $('#logTimeModal').modal('show');
            }
        }
    });
}

function checkTimer(){
    $.ajax({
        type: "POST",
        url: '/process/timer/check.php',
        data: '',
        dataType: 'json',
        success: function(response){
            console.log(response);
            if(response.status == 'active'){
                $('body').addClass('timer-bar--active');
                $('#top-bar-start').hide();
                $('#top-bar-stop').show();
                countUpFromTime(response.startTime);
            }else if(response.status == 'clear'){

            }
        }
    });
}

function removeTeam(userid){
    event.preventDefault();

    swal({
        title: "Are you sure?",
        text: "This user will have to be reinvited again to see this team.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "POST",
                url: '/process/team/remove.php',
                data: 'userid='+userid,
                dataType: 'json',
                success: function(response){
                    //console.log(response);
                    if(response.status == 'error'){
                        swal ( "Oops" ,  response.errorMessage ,  "error" );
                    }else if(response.status == 'success'){
                        if(typeof response.successRedirect !== 'undefined'){
                            window.location.href = response.successRedirect;
                        }
                        if(typeof response.successCallback !== 'undefined'){
                            console.log('Callback Function: '+response.successCallback);
                            window[response.successCallback](response.successCallbackParams);
                        }
                        if(typeof response.successMessage !== 'undefined'){
                            swal ( response.successMessage.title ,  response.successMessage.content ,  response.successMessage.style );
                        }
                    }
                }
            });
        }
    });


}

function minsStepUp(step){
    var currentMins = parseInt($('#time-input-mins').val());
    var currentHours = parseInt($('#time-input-hours').val());
    var seconds = (currentMins * 60) + (currentHours * 60 * 60);

    var stepSeconds = (parseInt(step)) * 60;

    var newSeconds = seconds + stepSeconds;

    var hours = secondsToUnit('hours',newSeconds);
    var mins = secondsToUnit('mins',newSeconds);

    mins = (Math.round(mins/step) * step) % 60;

    $('#time-input-hours').val(hours);
    $('#time-input-mins').val(mins);
}
function minsStepDown(step){
    var currentMins = parseInt($('#time-input-mins').val());
    var currentHours = parseInt($('#time-input-hours').val());
    var seconds = (currentMins * 60) + (currentHours * 60 * 60);

    var stepSeconds = (parseInt(step)) * 60;

    if(seconds <= 0){
        var newSeconds = 0;
    }else{
        var newSeconds = seconds - stepSeconds;
    }

    var hours = secondsToUnit('hours',newSeconds);
    var mins = secondsToUnit('mins',newSeconds);

    mins = (Math.round(mins/step) * step) % 60;

    $('#time-input-hours').val(hours);
    $('#time-input-mins').val(mins);
}

function hoursStepUp(step){
    var currentMins = parseInt($('#time-input-mins').val());
    var currentHours = parseInt($('#time-input-hours').val());
    var seconds = (currentMins * 60) + (currentHours * 60 * 60);

    var newSeconds = seconds + 3600;

    var hours = secondsToUnit('hours',newSeconds);
    var mins = secondsToUnit('mins',newSeconds);

    mins = (Math.round(mins/step) * step) % 60;

    $('#time-input-hours').val(hours);
    $('#time-input-mins').val(mins);
}
function hoursStepDown(step){
    var currentMins = parseInt($('#time-input-mins').val());
    var currentHours = parseInt($('#time-input-hours').val());
    var seconds = (currentMins * 60) + (currentHours * 60 * 60);

    if(seconds <= 3600){
        var newSeconds = 0;
    }else{
        var newSeconds = seconds - 3600;
    }

    var hours = secondsToUnit('hours',newSeconds);
    var mins = secondsToUnit('mins',newSeconds);

    mins = (Math.round(mins/step) * step) % 60;

    $('#time-input-hours').val(hours);
    $('#time-input-mins').val(mins);
}

function openLogItem(id){
	event.preventDefault();
	$.ajax({
        type: "POST",
        url: '/process/logs/get.php',
        data: 'logid='+id,
        dataType: 'json',
        success: function(response){
            console.log(response);
            if(response.status == 'success'){

				var totalMinutes = response.logData.seconds / 60;

				var hours = Math.floor(totalMinutes / 60);
				var mins = totalMinutes % 60;

				$("#logItemModal-hours").val(hours);
				$("#logItemModal-mins").val(mins);
				$("#logItemModal-job").val(response.logData.jobName + ' ('+response.logData.clientName+')');
				$("#logItemModal-description").val(response.logData.description);
				$("#delete-log-button").attr("data-logid",id);

				$('#logItemModal').modal('show');
            }else if(response.status == 'error'){
				swal ( "Oops" ,  response.message ,  "error" );
            }
        }
    });
}
$('#delete-log-button').on('click', function(event) {
    event.preventDefault();

	var logId = $(this).attr("data-logid");
	$(this).html('Deleting <i class="fas fa-spinner fa-pulse"></i>');

	$.ajax({
        type: "POST",
        url: '/process/logs/delete.php',
        data: 'logid='+logId,
        dataType: 'json',
        success: function(response){
            console.log(response);
			$(this).html('Delete');
            if(response.status == 'error'){
                swal ( "Oops" ,  response.message ,  "error" );
            }else if(response.status == 'success'){
                location.reload();
            }
        }
    });
});

function archiveJob(jobid){
	event.preventDefault();
	if(confirm("Are you sure? You cannot unarchive a job.")){
		$.ajax({
	        type: "POST",
	        url: '/process/jobs/archive.php',
	        data: 'jobid='+jobid,
	        dataType: 'json',
	        success: function(response){
	            console.log(response);
	            if(response.status == 'error'){
	                swal ( "Oops" ,  response.message ,  "error" );
	            }else if(response.status == 'success'){
	                location.reload();
	            }
	        }
	    });
	}
}

$('#manageBillingButton').on('click', function(event) {
    event.preventDefault();

	$(this).html('Manage Billing <i class="fas fa-spinner fa-pulse"></i>');

	$.ajax({
        type: "POST",
        url: '/process/team/getBillingPortalSession.php',
        dataType: 'json',
        success: function(response){
            console.log(response);
			$(this).html('Manage Billing');
            if(response.status == 'error'){
                swal ( "Oops" ,  response.message ,  "error" );
            }else if(response.status == 'success'){
                window.location.href = response.url;
            }
        }
    });
});
