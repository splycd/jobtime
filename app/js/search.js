/*
$('#search-bar').on('keyup', function(event) {
    event.preventDefault();
	var query = this.value;
    if(query.length >= 3){
		$('.top-bar__search').addClass('top-bar__search--processing');
		$.ajax({
	        type: "POST",
	        url: '/process/search.php',
	        data: 'query='+query,
	        dataType: 'json',
	        success: function(response){
	            console.log(response);
	            if(response.status == 'success'){

	            }else if(response.status == 'error'){

	            }
	        }
	    });
	}
});
*/


// Get the input box
let searchInput = document.getElementById('search-bar');

// Init a timeout variable to be used below
let timeout = null;


var processSearch = function(e){
	clearTimeout(timeout);
	$('.top-bar__search').addClass('top-bar__search--processing');
	$('body').removeClass('body-no-scroll');
	$('.search-overlay').removeClass('search-overlay--active');
	$('.search-results-container').removeClass('search-results-container--active');

    // Make a new timeout set to go off in 1000ms (1 second)
    timeout = setTimeout(function () {
		var query = searchInput.value;
		if(query.length >= 3){
			$.ajax({
		        type: "POST",
		        url: '/process/search.php',
		        data: 'query='+query,
		        dataType: 'json',
		        success: function(response){
		            //console.log(response);
		            if(response.status == 'success'){
						console.log(response.results);

						$(".search-results__grid").html("");

						var results = response.results;
						results.forEach(function(result){
							var clientName = result.name;
							var clientId = result.clientid;
							var totalJobs = result.totalJobs;
							var totalArchive = result.totalArchive;

							var jobs = '';
							var archive = '';

							if(result.searchJobsCount > 0){
								var searchJobs = result.searchJobs;
								searchJobs.forEach(function(job){
									var jobName = job.name;
									var jobId = job.jobid;

									jobs += '<a href="/jobs/'+jobId+'"><span class="search-results__jobs__title">'+jobName+'</span><span class="search-results__jobs__type">Active Job</span></a>';
								});
							}
							if(result.searchArchiveCount > 0){
								var searchArchive = result.searchArchive;
								searchArchive.forEach(function(job){
									var jobName = job.name;
									var jobId = job.jobid;

									jobs += '<a href="/jobs/'+jobId+'"><span class="search-results__jobs__title">'+jobName+'</span><span class="search-results__jobs__type">Archived</span></a>';
								});
							}
							console.log(jobs);
							var jobList = '';

							if(result.searchJobsCount > 0 || result.searchArchiveCount > 0){
								jobList = '<div class="search-results__jobs">'+jobs+archive+'</div>';
							}

							$(".search-results__grid").append('<div class="search-results__grid-item"><a href="/clients/'+clientId+'" class="search-results__client"><span>'+clientName+'</span><ul><li>Jobs: '+totalJobs+'</li><li>Archived: '+totalArchive+'</li></ul></a>'+jobList+'</div>');
						});

						$('.top-bar__search').removeClass('top-bar__search--processing');
						$('body').addClass('body-no-scroll');
						$('.search-overlay').addClass('search-overlay--active');
						$('.search-results-container').addClass('search-results-container--active');
		            }else if(response.status == 'error'){
						$('.top-bar__search').removeClass('top-bar__search--processing');
						$('body').removeClass('body-no-scroll');
						$('.search-overlay').removeClass('search-overlay--active');
						$('.search-results-container').removeClass('search-results-container--active');
		            }
		        }
		    });
		}else{
			$('.top-bar__search').removeClass('top-bar__search--processing');
		}
    }, 500);
};

searchInput.addEventListener('keyup', processSearch);
searchInput.addEventListener('click', processSearch);

$('.search-overlay').on('click', function(event) {
    event.preventDefault();
	$('body').removeClass('body-no-scroll');
	$('.search-overlay').removeClass('search-overlay--active');
	$('.search-results-container').removeClass('search-results-container--active');
});
