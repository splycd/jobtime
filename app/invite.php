<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';

if(isset($_SESSION['userid'])){
	header('Location: /');
	exit;
}

$invite = $database->get('userInvite',[
	'[>]teams'=>'teamid'
],[
	'userInvite.token',
	'userInvite.email',
	'teams.teamid',
	'teams.name'
],[
	'token'=>$_GET['id']
]);



$PAGE_TITLE = 'Join '.$invite['name'];
$PAGE_SLUG = 'login';
//checkLogin();
//checkTeam();

$wallpaper = getWallpapers();
?>

<!doctype html>
<html lang="en">
    <?php getInclude('head.php');?>
    <body>
        <div class="account-structure">
            <div class="account-structure__main">
                <div class="account-structure__single-nav">
                    <a href="/" class="account-structure__branding">
                        <img class="logo-dark" src="/assets/logo-dark.svg">
                        <img class="logo-light" src="/assets/logo-light.svg">
                    </a>
                </div>
                <section class="text-center">
                    <h1>Join <?php echo $invite['name'];?></h1>
					<p>You've been invited to join <?php echo $invite['name'];?> on JobTime. Create your account to get started!</p>
                </section>
                <section>
                    <form class="process-form" method="post" action="/process/user/inviteRegister.php">
						<input type="hidden" name="token" value="<?php echo $_GET['id'];?>">
						<div class="form-group">
							<label>First Name</label>
							<input class="input-text" type="text" id="firstname" name="firstname" placeholder="E.g. John">
						</div>
						<div class="form-group">
							<label>Last Name</label>
							<input class="input-text" type="text" id="lastname" name="lastname" placeholder="E.g. Smith">
						</div>
						<div class="form-group">
							<label>Email Address</label>
							<input class="input-text" type="email" id="email" name="email" placeholder="E.g. example@address.com" value="<?php echo $invite['email'];?>">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input class="input-text" type="password" id="password1" name="password1" placeholder="***********">
						</div>
						<div class="form-group">
							<label>Re-enter Password</label>
							<input class="input-text" type="password" id="password2" name="password2" placeholder="***********">
						</div>
                        <div class="form-group text-center">
                            <button type="submit" class="button button--red">Create Account</button>
                        </div>
                    </form>
                </section>
            </div>
			<div class="account-structure__background" style="background-image: url(/assets/wallpapers/<?php echo $wallpaper[0];?>)">
                <a target="_blank" href="<?php echo $wallpaper[2];?>">Photo by <?php echo $wallpaper[1];?> on Unsplash</a>
            </div>
        </div>
        <?php getInclude('scripts.php');?>
    </body>
</html>
