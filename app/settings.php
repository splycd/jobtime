<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
$PAGE_TITLE = 'Team Settings';
$PAGE_SLUG = 'settings';
checkLogin();
checkTeam();
$teamSettings = teamSettings();

$user = $database->get('users','*',[
    'userid'=>$_SESSION['userid']
]);
$team = $database->get('teams','*',[
    'teamid'=>$_SESSION['teamid']
]);
?>

<!doctype html>
<html lang="en">
    <?php getInclude('head.php');?>
    <body>
        <div class="structure">
            <?php getInclude('sidebar.php');?>
            <div class="structure__main">
                <?php getInclude('timer.php');?>
                <?php getInclude('top-bar.php');?>
				<!--
				<section>
					<div class="container">
						<button id="manageBillingButton" type="button" class="button">Manage Billing</button>
					</div>
				</section>
			-->
				<section>
					<div class="container container--narrow">
						<div class="heading-action">
							<?php
							$memberCount = $database->count('users',['teamid'=>$_SESSION['teamid']]);
							if(checkSubscription() == 'free'){
							?>
							<h2 class="m-0">Members <span class="badge"><?php echo $memberCount;?> / 5</span></h2>
							<?php }else{ ?>
							<h2 class="m-0">Members <span class="badge"><?php echo $memberCount;?></span></h2>
							<?php } ?>
							<button class="button button--red" data-toggle="modal" data-target="#inviteUser">Add Member</button>
                        </div>
						<div class="object-table mb-4">

							<div class="object-table__row">
								<div class="object-table__row__body object-table__row__body--users">
									<div class="object-table-users__avatar" style="background-image:url(<?php echo getGravatar($user['email']);?>)">
									</div>
									<div class="object-table-users__name">
										<?php echo $user['firstname'];?> <?php echo $user['lastname'];?>
									</div>
									<div class="object-table-users__email">
										<?php echo $user['email'];?>
									</div>
								</div>
							</div>

							<?php
							$teamUsers = $database->select('users',[
								'email',
								'firstname',
								'lastname',
								'authority',
							],[
								'teamid'=>$_SESSION['teamid'],
								'userid[!]'=>$_SESSION['userid'],
							]);

							foreach($teamUsers as $teamUser){
								renderTeamUser($teamUser);
							}

							?>

						</div>
					</div>
					<?php
					$inviteCount = $database->count('userInvite',['teamid'=>$_SESSION['teamid']]);
					if($inviteCount > 0){
					?>
					<div class="container container--narrow">
						<h3 class="mb-2">Pending Invites</h3>
						<div class="object-table">

							<?php
							$invites = $database->select('userInvite',[
								'email'
							],[
								'teamid'=>$_SESSION['teamid']
							]);

							foreach($invites as $invite){
								renderTeamInvite($invite);
							}

							?>
						</div>
					</div>
					<?php } ?>
				</section>
            </div>
        </div>

        <div class="modal" id="inviteUser" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Invite User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
						<?php if($memberCount >= 5){ ?>
							<div class="row">
                                <div class="col-12">
                                    <h2>You've found a Pro feature!</h2>
									<p>You're currently limited to 5 Team Members, upgrade to JobTime Pro for unlimited Team Members.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <a href="#" class="button button--red button--fw">Upgrade Now</a>
                                </div>
                            </div>
						<?php }else{ ?>
	                        <form class="process-form" method="post" action="/process/user/invite.php">
	                            <div class="row">
	                                <div class="col-12">
	                                    <div class="form-group">
	                                        <label>Email Address</label>
	                                        <input type="email" class="input-text" id="email" name="email" required>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-12">
	                                    <button type="submit" class="button button--red button--fw">Invite User</button>
	                                </div>
	                            </div>
	                        </form>
						<?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <?php getInclude('scripts.php');?>
    </body>
</html>
