<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
$PAGE_TITLE = 'Account';
$PAGE_SLUG = 'account';
checkLogin();
//checkTeam();

$user = $database->get('users','*',[
    'userid'=>$_SESSION['userid']
]);

$wallpaper = getWallpapers();
?>

<!doctype html>
<html lang="en">
    <?php getInclude('head.php');?>
    <body>
        <div class="account-structure">
            <div class="account-structure__main">
				<div class="account-structure__split-nav">
                    <a href="/" class="account-structure__branding">
                        <img class="logo-dark" src="/assets/logo-dark.svg">
                        <img class="logo-light" src="/assets/logo-light.svg">
                    </a>
                    <a href="/" class="account-structure__split-nav__button"><i class="fas fa-chevron-left"></i> Go Back</a>
                </div>
                <section class="text-center text-md-left">
                    <h1>Hi, <?php echo $user['firstname'];?></h1>
                </section>
                <section class="text-center text-md-left">
                    <a href="/process/user/logout.php" class="button button--red">Log out</a>
                    <a href="/process/user/logoutAll.php" class="button button--transparent">Log out of all devices</a>
                </section>
            </div>
			<div class="account-structure__background" style="background-image: url(/assets/wallpapers/<?php echo $wallpaper[0];?>)">
                <a target="_blank" href="<?php echo $wallpaper[2];?>">Photo by <?php echo $wallpaper[1];?> on Unsplash</a>
            </div>
        </div>
        <?php getInclude('scripts.php');?>
    </body>
</html>
