<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
$PAGE_TITLE = 'Jobs';
$PAGE_SLUG = 'jobs';
checkLogin();
checkTeam();
$teamSettings = teamSettings();

$user = $database->get('users','*',[
    'userid'=>$_SESSION['userid']
]);
$team = $database->get('teams','*',[
    'teamid'=>$_SESSION['teamid']
]);
?>

<!doctype html>
<html lang="en">
    <?php getInclude('head.php');?>
    <body>
        <div class="structure">
            <?php getInclude('sidebar.php');?>
            <div class="structure__main">
                <?php getInclude('timer.php');?>
                <?php getInclude('top-bar.php');?>
				<section>
					<div class="container">
						<div class="heading-action">
                            <h1 class="m-0">Jobs <span class="badge"> / 5</span></h1>
                            <button class="button button--red" data-toggle="modal" data-target="#createJobModal">Create Job</button>
                        </div>
						<div class="object-table">

							<?php
							$jobs = $GLOBALS['database']->select('jobs',[
                                '[>]clients'=>'clientid'
                            ],[
                                'jobs.name(jobname)',
                                'jobs.jobid',
                                'jobs.budget [Int]',
                                'clients.name(clientname)',
                            ],[
                                'jobs.teamid'=>$_SESSION['teamid'],
								'ORDER'=>['jobs.dateUpdated'=>'DESC'],
                            ]);
                            foreach($jobs as $job){
                                renderJobTable($job);
                            }
                            ?>

						</div>
					</div>
				</section>
            </div>
        </div>

        <div class="modal" id="createJobModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Job</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="process-form" method="post" action="/process/jobs/create.php">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Client</label>
                                        <select class="input-dropdown" name="clientid" required>
                                            <option value="">Please Select</option>
                                            <?php
                                            $clients = $GLOBALS['database']->select('clients',[
                                                'name',
                                                'clientid',
                                            ],[
                                                'teamid'=>$_SESSION['teamid']
                                            ]);
                                            foreach($clients as $client){
                                                echo'<option value="'.$client['clientid'].'">'.$client['name'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Job Name</label>
                                        <input type="text" class="input-text" id="jobName" name="jobName" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Hours Budget</label>
                                        <input type="number" class="input-text" id="budget" name="budget">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="button button--red button--fw">Create Job</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php getInclude('scripts.php');?>
    </body>
</html>
