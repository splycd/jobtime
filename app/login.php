<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
$PAGE_TITLE = 'Log in';
$PAGE_SLUG = 'login';
//checkLogin();
//checkTeam();

$wallpaper = getWallpapers();
?>

<!doctype html>
<html lang="en">
    <?php getInclude('head.php');?>
    <body>
        <div class="account-structure">
            <div class="account-structure__main">
                <div class="account-structure__single-nav">
                    <a href="/" class="account-structure__branding">
                        <img class="logo-dark" src="/assets/logo-dark.svg">
                        <img class="logo-light" src="/assets/logo-light.svg">
                    </a>
                </div>
                <section class="text-center">
                    <h1>Log in</h1>
					<p>Log in to access your dashboard, or if you're new here, get started by creating a new Team.</p>
                </section>
                <section>
                    <form class="process-form" method="post" action="/process/user/login.php">
                        <div class="form-group">
                            <label>Email Address</label>
                            <input class="input-text" type="email" id="email" name="email" placeholder="example@address.com">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input class="input-text" type="password" id="password" name="password" placeholder="***********">
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="button button--red">Log in</button>
                        </div>
                        <div class="form-group text-center">
                            <a href="/register" class="heading-link">Create a Team <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </form>
                </section>
            </div>
            <div class="account-structure__background" style="background-image: url(/assets/wallpapers/<?php echo $wallpaper[0];?>)">
                <a target="_blank" href="<?php echo $wallpaper[2];?>">Photo by <?php echo $wallpaper[1];?> on Unsplash</a>
            </div>
        </div>
        <?php getInclude('scripts.php');?>
    </body>
</html>
