<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';

if(!empty($_GET['id'])){
	$tokenCount = $database->count('userVerify',['token'=>$_GET['id']]);
	if($tokenCount !== 1){
		header('Location: /');
		exit;
	}else{
		$database->delete('userVerify',['token'=>$_GET['id']]);
		header('Location: /');
		exit;
	}
}

$PAGE_TITLE = 'Verify';
$PAGE_SLUG = 'verify';

if(isset($_SESSION['userid'])){
	$user = $database->get('users','*',[
	    'userid'=>$_SESSION['userid']
	]);
}

$wallpaper = getWallpapers();
?>

<!doctype html>
<html lang="en">
    <?php getInclude('head.php');?>
    <body>
        <div class="account-structure">
			<div class="account-structure__main">
				<?php if(isset($_SESSION['userid'])){?>
					<div class="account-structure__split-nav">
                    	<a href="/" class="account-structure__branding">
                        	<img class="logo-dark" src="/assets/logo-dark.svg">
                        	<img class="logo-light" src="/assets/logo-light.svg">
                    	</a>
                    	<a href="/process/user/logout.php" class="account-structure__split-nav__button"><i class="fas fa-chevron-left"></i> Log Out</a>
                	</div>
				<?php }else{?>
					<div class="account-structure__single-nav">
	                    <a href="/" class="account-structure__branding">
	                        <img class="logo-dark" src="/assets/logo-dark.svg">
	                        <img class="logo-light" src="/assets/logo-light.svg">
	                    </a>
	                </div>
				<?php }?>
                <section class="text-center">
                    <h1>Check your email!</h1>
					<p>We need you to verify your email address before you continue.</p>
					<?php if(isset($_SESSION['userid'])){?>
					<p>Check your email (<?php echo $user['email'];?>) for a link from us!</p>
					<?php }?>
                </section>
            </div>
			<div class="account-structure__background" style="background-image: url(/assets/wallpapers/<?php echo $wallpaper[0];?>)">
                <a target="_blank" href="<?php echo $wallpaper[2];?>">Photo by <?php echo $wallpaper[1];?> on Unsplash</a>
            </div>
        </div>
        <?php getInclude('scripts.php');?>
    </body>
</html>
