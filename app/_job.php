<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
$PAGE_TITLE = 'Jobs';
$PAGE_SLUG = 'jobs';
checkLogin();
checkTeam();
$teamSettings = teamSettings();

$user = $database->get('users','*',[
    'userid'=>$_SESSION['userid']
]);
$team = $database->get('teams','*',[
    'teamid'=>$_SESSION['teamid']
]);

$job = $database->get('jobs','*',[
    'jobid'=>$_GET['id']
]);
?>

<!doctype html>
<html lang="en">
    <?php getInclude('head.php');?>
    <body>
        <div class="structure">
            <?php getInclude('sidebar.php');?>
            <div class="structure__main">
                <?php getInclude('timer.php');?>
                <?php getInclude('top-bar.php');?>
                <section>
                    <div class="container text-center text-md-left">
                        <a href="/jobs" class="heading-link"><i class="fas fa-chevron-left"></i> Jobs</a>
                        <h1><?php echo $job['name'];?></h1>
                    </div>
                </section>
                <section>
                    <div class="container container--narrow">
						<h2 class="text-center">Activity</h2>
						<div class="log-grid log-grid--users">
							<?php
							$logs = $database->select('logs',[
								'[>]jobs'=>['jobid'=>'jobid'],
								'[>]clients'=>['clientid'=>'clientid'],
							],[
								'jobs.name(jobname)',
								'clients.name(clientname)',
								'logs.seconds [Int]',
								'logs.description',
								'logs.userid',
								'logs.id',
							],[
								'logs.jobid'=>$_GET['id'],
								'ORDER'=>['logs.dateCreated'=>'DESC']
							]);

							//var_dump($logs);

							foreach($logs as $log){
								renderLog($log);
							}
							?>
						</div>
                    </div>
                </section>
            </div>
        </div>

        <div class="modal" id="createJobModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Job</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="process-form" method="post" action="/process/jobs/create.php">
                            <input type="hidden" name="clientid" value="<?php echo $_GET['id'];?>">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Job Name</label>
                                        <input type="text" class="input-text" id="jobName" name="jobName" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Hours Budget</label>
                                        <input type="number" class="input-text" id="budget" name="budget">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="button button--red button--fw">Create Job</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
		<?php getInclude('log-item-modal.php');?>
        <?php getInclude('scripts.php');?>
    </body>
</html>
