<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
$PAGE_TITLE = 'Dashboard';
$PAGE_SLUG = 'index';
checkLogin();
checkTeam();
$teamSettings = teamSettings();

$user = $database->get('users','*',[
    'userid'=>$_SESSION['userid']
]);
$team = $database->get('teams','*',[
    'teamid'=>$_SESSION['teamid']
]);
?>

<!doctype html>
<html lang="en">
    <?php getInclude('head.php');?>
    <body>
        <div class="structure">
            <?php getInclude('sidebar.php');?>
            <div class="structure__main">
                <?php getInclude('timer.php');?>
                <?php getInclude('top-bar.php');?>
                <section>
                    <div class="container">
                        <div class="heading-action">
                            <h2>Recent Jobs</h2>
                            <a href="/jobs" class="heading-link">View all <i class="fas fa-chevron-right"></i></a>
                        </div>
                        <div class="object-grid object-grid--cols-4">
                            <?php
                            $jobs = $GLOBALS['database']->select('jobs',[
                                '[>]clients'=>'clientid'
                            ],[
                                'jobs.name(jobname)',
                                'jobs.jobid',
                                'jobs.budget [Int]',
                                'clients.name(clientname)',
                            ],[
                                'jobs.teamid'=>$_SESSION['teamid'],
                                'ORDER'=>['jobs.dateUpdated'=>'DESC'],
								'LIMIT'=>4
                            ]);
                            $count = 0;
                            foreach($jobs as $job){
                                $count ++;
                                renderJob($job);
                            }
                            ?>
                        </div>
                    </div>
                </section>
				<?php
				$weekDate = date("Y-m-d",strtotime("-7 days")).' 00:00:00';
				$logs = $database->select('logs',[
					'[>]jobs'=>['jobid'=>'jobid'],
					'[>]clients'=>['clientid'=>'clientid'],
				],[
					'jobs.name(jobname)',
					'clients.name(clientname)',
					'logs.id',
					'logs.seconds [Int]',
					'logs.description',
					'logs.dateCreated',
				],[
					'AND'=>[
						'logs.dateCreated[>=]'=>$weekDate,
						'logs.userid'=>$_SESSION['userid'],
					],
					'ORDER'=>['logs.dateCreated'=>'DESC']
				]);

				$todayLogs = array();
				$yesterdayLogs = array();
				$days2Logs = array();
				$days3Logs = array();
				$days4Logs = array();
				$days5Logs = array();
				$days6Logs = array();

				foreach($logs as $log){
					$logDate = date("Y-m-d H:i:s",strtotime($log['dateCreated']));
					//var_dump(date("Y-m-d",strtotime($log['dateCreated'])));
					if($logDate >= date("Y-m-d",strtotime("0 days")).' 00:00:00'){
						array_push($todayLogs, $log);
					}else if($logDate >= date("Y-m-d",strtotime("-1 days")).' 00:00:00' && $logDate < date("Y-m-d",strtotime("0 days")).' 00:00:00'){
						array_push($yesterdayLogs, $log);
					}else if($logDate >= date("Y-m-d",strtotime("-2 days")).' 00:00:00' && $logDate < date("Y-m-d",strtotime("-1 days")).' 00:00:00'){
						array_push($days2Logs, $log);
					}else if($logDate >= date("Y-m-d",strtotime("-3 days")).' 00:00:00' && $logDate < date("Y-m-d",strtotime("-2 days")).' 00:00:00'){
						array_push($days3Logs, $log);
					}else if($logDate >= date("Y-m-d",strtotime("-4 days")).' 00:00:00' && $logDate < date("Y-m-d",strtotime("-3 days")).' 00:00:00'){
						array_push($days4Logs, $log);
					}else if($logDate >= date("Y-m-d",strtotime("-5 days")).' 00:00:00' && $logDate < date("Y-m-d",strtotime("-4 days")).' 00:00:00'){
						array_push($days5Logs, $log);
					}else if($logDate >= date("Y-m-d",strtotime("-6 days")).' 00:00:00' && $logDate < date("Y-m-d",strtotime("-5 days")).' 00:00:00'){
						array_push($days6Logs, $log);
					}
				}

				if(count($logs) > 0){
				?>
	                <section>
	                    <div class="container container--narrow">
							<h2 class="text-center">My Activity (Last 7 days)</h2>

							<?php if(count($todayLogs) > 0){?>
							<h6 class="logs-header">Today</h6>
							<div class="log-grid">
								<?php
								foreach($todayLogs as $log){
									renderLog($log);
								}
								?>
							</div>
							<?php }?>

							<?php if(count($yesterdayLogs) > 0){?>
							<h6 class="logs-header">Yesterday</h6>
							<div class="log-grid">
								<?php
								foreach($yesterdayLogs as $log){
									renderLog($log);
								}
								?>
							</div>
							<?php }?>

							<?php if(count($days2Logs) > 0){?>
							<h6 class="logs-header">2 Days Ago</h6>
							<div class="log-grid">
								<?php
								foreach($days2Logs as $log){
									renderLog($log);
								}
								?>
							</div>
							<?php }?>

							<?php if(count($days3Logs) > 0){?>
							<h6 class="logs-header">3 Days Ago</h6>
							<div class="log-grid">
								<?php
								foreach($days3Logs as $log){
									renderLog($log);
								}
								?>
							</div>
							<?php }?>

							<?php if(count($days4Logs) > 0){?>
							<h6 class="logs-header">4 Days Ago</h6>
							<div class="log-grid">
								<?php
								foreach($days4Logs as $log){
									renderLog($log);
								}
								?>
							</div>
							<?php }?>

							<?php if(count($days5Logs) > 0){?>
							<h6 class="logs-header">5 Days Ago</h6>
							<div class="log-grid">
								<?php
								foreach($days5Logs as $log){
									renderLog($log);
								}
								?>
							</div>
							<?php }?>

							<?php if(count($days6Logs) > 0){?>
							<h6 class="logs-header">6 Days Ago</h6>
							<div class="log-grid">
								<?php
								foreach($days6Logs as $log){
									renderLog($log);
								}
								?>
							</div>
							<?php }?>
	                    </div>
	                </section>
				<?php }else{?>
					<section>
	                    <div class="container container--narrow text-center">
							<h2>Let's get logging!</h2>
							<p>You haven't logged any time in the past 7 days.</p>
							<button class="button button--yellow" data-toggle="modal" data-target="#logTimeModal">Log Time</button>
						</div>
					</section>
				<?php }?>
            </div>
        </div>

        <?php getInclude('log-item-modal.php');?>
        <?php getInclude('scripts.php');?>
    </body>
</html>
