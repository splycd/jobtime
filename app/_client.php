<?php
require_once dirname($_SERVER['DOCUMENT_ROOT']).'/execute.php';
$PAGE_TITLE = 'Clients';
$PAGE_SLUG = 'clients';
checkLogin();
checkTeam();
$teamSettings = teamSettings();

$user = $database->get('users','*',[
    'userid'=>$_SESSION['userid']
]);
$team = $database->get('teams','*',[
    'teamid'=>$_SESSION['teamid']
]);

$client = $database->get('clients','*',[
    'clientid'=>$_GET['id']
]);
?>

<!doctype html>
<html lang="en">
    <?php getInclude('head.php');?>
    <body>
        <div class="structure">
            <?php getInclude('sidebar.php');?>
            <div class="structure__main">
                <?php getInclude('timer.php');?>
                <?php getInclude('top-bar.php');?>
                <section>
                    <div class="container text-center text-md-left">
                        <a href="/clients" class="heading-link"><i class="fas fa-chevron-left"></i> Clients</a>
                        <h1><?php echo $client['name'];?></h1>
                    </div>
                </section>
				<section>
					<div class="container">
						<div class="heading-action">
                            <h2 class="m-0">Jobs</h2>
                            <button class="button button--red" data-toggle="modal" data-target="#createJobModal">Create Job</button>
                        </div>
						<div class="object-table">

							<?php
							$jobCount = $database->count('jobs',['clientid'=>$_GET['id']]);
							if($jobCount < 1){
								echo'<div class="object-table__row-placeholder">No active Jobs.</div>';
							}else{
								$jobs = $database->select('jobs',[
	                                '[>]clients'=>'clientid'
	                            ],[
	                                'jobs.name(jobname)',
	                                'jobs.jobid',
	                                'jobs.budget [Int]',
	                            ],[
	                                'clientid'=>$_GET['id']
	                            ]);
	                            foreach($jobs as $job){
	                                renderJobTable($job);
	                            }
							}
                            ?>

						</div>
					</div>
				</section>
            </div>
        </div>

        <div class="modal" id="createJobModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Job</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="process-form" method="post" action="/process/jobs/create.php">
                            <input type="hidden" name="clientid" value="<?php echo $_GET['id'];?>">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Job Name</label>
                                        <input type="text" class="input-text" id="jobName" name="jobName" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Hours Budget</label>
                                        <input type="number" class="input-text" id="budget" name="budget">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="button button--red button--fw">Create Job</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php getInclude('scripts.php');?>
    </body>
</html>
